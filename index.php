<!DOCTYPE html>
<html lang="en" ng-app="app">
	<head>
		<meta charset="utf-8">
		<title><?php echo $_SERVER['HTTP_HOST']; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootswatch/3.2.0/yeti/bootstrap.min.css">
		<style>
			html {
				position: relative;
				min-height: 100%;
			}
			body {
				padding-top: 60px;
				margin-bottom: 40px;
			}
			.table > tbody > tr > td {
				padding: 0;
			}
			.table a {
				display: block;
				padding: 8px;
			}
			.table a:hover,
			.table a:focus {
				text-decoration: none;
			}
			.footer {
				position: fixed;
				bottom: 0;
				width: 100%;
				height: 45px;
				line-height: 45px;
				background-color: #f5f5f5;
				border-top: 1px solid #ddd;
			}
		</style>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.min.js"></script>
		<?php

			$directories = array();

			if ($root = opendir('.')) {

				$blacklist = array('.', '..', '.git', '.editorconfig', '.htaccess', 'index.php', 'index.html', 'index.htm', 'favicon.ico');

				while (false !== ($file = readdir($root))) {

					if (!in_array($file, $blacklist)) {
						array_push($directories, $file);
					}

				}

				closedir($root);

			}

			sort($directories);

		?>
		<script>
			var app = angular.module('app', []);
			app.controller('MainController', function ($scope) {
				$scope.directories = [
					<?php
						foreach ($directories as $directory) {
							echo "{'name': '$directory' },";
						}
					?>
				];
			});
		</script>
	</head>

	<body ng-controller="MainController">

		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="//<?php echo $_SERVER['HTTP_HOST']; ?>"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> <?php echo $_SERVER['HTTP_HOST']; ?></a>
				</div>

				<div class="collapse navbar-collapse pull-right">
					<form class="navbar-form navbar-left" role="form">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Search" ng-model="query" autofocus>
						</div>
					</form>
					<ul class="nav navbar-nav">
						<li class="active"><a href="//<?php echo $_SERVER['HTTP_HOST']; ?>"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Dashboard</a></li>
						<li><a href="info.php"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> phpinfo()</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="container-fluid">

			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Index of /</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="directory in directories | filter:query">
							<td>
								<a href="{{directory.name}}"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;{{directory.name}}</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>

		<div class="footer">
			<div class="container-fluid">
				<p class="text-muted text-center"><?php echo strip_tags($_SERVER['SERVER_SIGNATURE']); ?></p>
			</div>
		</div>

	</body>
</html>
